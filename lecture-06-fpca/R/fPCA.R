#############################################
# Standard functional PCA, uses fda package #
#############################################

# choose basis (cubic B-spline functions)
bb <- create.bspline.basis(age, norder=4) 

# create functional data object
growth.fd <- Data2fd(y=height, argvals=age, basis=bb)

# functional PCA
growth.pca <- pca.fd(growth.fd, nharm=2, centerfns=T)

# calculate proportion of variance -> 2 principal components are reasonable
round(growth.pca$values/sum(growth.pca$values),3)

# plot the principal components
plot(growth.pca$harmonics,
     xlab = "Age", ylab = "", 
     col = c("darkgreen", "blue"), lty = 1, lwd = 2.5)
legend("bottomleft", legend = c("PC 1", "PC 2"), 
       col = c("darkgreen", "blue"), lwd = 2.5,
       bty = "n")

# plot mean plus effects of first and second functional principal components
# my.plot.pca.fd=plot.pca.fd + allow for individual main, axis lim/lab, ...
my.plot.pca.fd(growth.pca, expand=30, pointplot=T, harm=1, ylim=c(60, 200),
     xlab="Age", ylab="Height", main="Effect of PC 1")
my.plot.pca.fd(growth.pca, expand=30, pointplot=T, harm=2, ylim=c(60, 200),
     xlab="Age", ylab="Height", main="Effect of PC 2")

# plot scores
plot(growth.pca$scores[,1], growth.pca$scores[,2],
     pch=20, col=ifelse(sex=="f", "red","blue"),
     xlab="Score PC 1", ylab="Score PC 2")
legend("topleft", legend=c("girls", "boys"), col=c("red","blue"), pch=20)
