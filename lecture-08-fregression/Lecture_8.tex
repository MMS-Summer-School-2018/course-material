\documentclass[
%handout
]{beamer}

\newcommand{\chapternr}{8}

\newcommand{\semester}{Oberwolfach}

%\usepackage{../beamerLMU/beamerthemelmu}
% \documentclass[display]{FLslides}


%\input{../titelfolie}
\input{defs}

% Coding and Languages
\usepackage[utf8]{inputenc} %Codierung für Linux
\usepackage[ngerman, english]{babel}

\beamertemplatenavigationsymbolsempty

%\usetikzlibrary{external}
%\tikzexternalize[prefix=figs/]


\setbeamertemplate{caption}{\insertcaption} 
\setbeamertemplate{caption label separator}{}
\setbeamertemplate{blocks}[rounded][shadow=true]

\setbeamertemplate{section in toc}[sections numbered]
\setbeamertemplate{subsection in toc}[subsections numbered]
\AtBeginSection[]
{
\begin{frame}[plain,noframenumbering]
\frametitle{Lecture \chapternr{} -- Overview}
\tableofcontents[currentsection,subsectionstyle=show/show/hide]
\end{frame}
}


\usepackage{arydshln}
\usepackage[round]{natbib}

\sloppy

\newcommand{\EEG}{\textnormal{\tiny \textsc{EEG}}}
\newcommand{\EMG}{\textnormal{\tiny \textsc{EMG}}}

\newcommand{\NIR}{\textnormal{\tiny \textsc{NIR}}}
\newcommand{\UVVIS}{\textnormal{\tiny \textsc{UV}}}
\newcommand{\ho}{\textnormal{\tiny \textsc{h2o}}}
\newcommand{\heat}{\textnormal{\tiny \textsc{heat}}}

\title{\textbf{MMS Summer School 2018}\\[0.5cm]Lecture \chapternr:\\Regression for Functional Data}
\author{Almond Stöcker\\ {\scriptsize based on slides by Sarah Brockhaus and David R\"ugamer\newline \texttt{github.com/davidruegamer/StatisticalComputing2017} } }
\institute{LMU München}
\date{\semester}

\setbeamertemplate{footline}[text line]{%
  \parbox{\textwidth}{\vspace*{-8pt}MMS Summer School 2018 -- \semester \hfill Lecture \chapternr ~-- Slide \insertframenumber/\inserttotalframenumber}}

\setbeamercolor{alerted text}{fg=lmuGruen}

%\newcommand{\bi}{\begin{itemize}}
%\newcommand{\ei}{\end{itemize}}
%\newcommand{\ra}{\rightarrow}

\input{newCommands.tex} %$

\begin{document}

\begin{frame}[plain, noframenumbering]
	\titlepage
\end{frame}

% ~~~~~~~~~~~ ~~~~~~~~~~~~~~~ ~~~~~~~~~~~~~~ ~~~~~~~~~~~~~~

\section{Introduction}
\subsection{Functional regression problems}

\begin{frame}
\frametitle{\secname}
\framesubtitle{\subsecname}

Analyze, interpret and test effects of covariates on\newline response observations involving functional data.
\begin{itemize}
	\item different generalizations from usual scalar model types, such as (generalized) linear regression, kernel regression,...
	\item overviews of functional regression:\newline \cite{morris2015}, \cite{greven2017}
\end{itemize}
\vspace{.7cm}
\centering
\begin{tikzpicture}[sibling distance=10em,
every node/.style = {shape=rectangle, rounded corners,
	draw, align=center,
	top color=white, bottom color=blue!20}]]
%% Draw system flow diagram
\begin{scope} [very thick, node distance=1cm,>=stealth', block/.style={rectangle,draw,fill=cyan!20}, comp/.style={circle,draw,fill=orange!40}] %xshift=-7.5cm,yshift=-5cm,
\node [block] (re)					{function-on-function};
%\node [comp]	 (cb)	[above=of re]			{B}  edge [->] (re);
%\node [comp]	 (ca1)	[above=of cb,xshift=-0.8cm]	{A1} edge [->] (cb);
%\node [comp]	 (ca2)	[right=of ca1]			{A2} edge [->] (cb);
\node [block] (s1)	[above=of re, xshift=-1.8cm]		{function-on-scalar} edge (re);
\node [block] (s2)	[right=of s1]		{scalar-on-function} edge (re);
\end{scope}
\end{tikzpicture}

\end{frame}

% ------------ ---------------- ----------------- ---------------


\subsection{Example data: Spectral data of fossil fuels}

\frame{
	\frametitle{Example data: Spectral data of fossil fuels}
	\textbf{Goal: predict heat value} $y$ using the spectral measurements of NIR (near infrared) and UV (ultraviolet-visible) spectra % $x_\NIR (s_\NIR)$ and $x_\UVVIS (s_\UVVIS)$  
	%$x_i(s)$\\ 
	% Scalar-on-function-regression: $\color{black}{y_i} = \mu + \int {\color{black}{x_i(s)}} \beta(s) \drm s + \eps_i$ 
	%Spectral data of fossil fuels to predict heat value 
	%\begin{columns}
	%\begin{column}[c]{0.55\textwidth}
	%    \begin{figure}
	%    \includegraphics[width=\columnwidth, page=4]{figures/NIR_UVVIS}
	%    \end{figure}
	%\end{column}
	%\begin{column}{0.45\textwidth}
	%    \begin{figure}
	%    \includegraphics[width=\columnwidth, page=1]{figures/NIR_UVVIS}
	%    \end{figure}
	%\end{column}\end{columns}
	
	\includegraphics[width = 0.99\textwidth]{figures/fuel01-1}
	
	\bigskip
	\begin{flushright} \citep{brockhaus2015} \end{flushright}
}

% -------------- -------------------- ---------------------- -------------------


%%%%%%%%%%%%%%%%% generic model

\section{Functional additive regression models}
\subsection{Generic model formulation}

\begin{frame}
	\frametitle{\secname}
	\framesubtitle{\subsecname}
	\begin{itemize}
		\item functional response $Y(t)$, $t \in \mathcal{T} = [T_1, T_2]$
		\item vector of covariates $\bm{x}$ containing functional covariates $x(s)$ and scalar covariates $z$
	\end{itemize}
	
	\begin{block}{Generic model}
		\begin{equation}
		\nonumber
		\mathbb{E}\left(Y(t)\mid \bm{x}\right) =  h(\bm{x}, t) = {\sum}_{j} \, {h_j(\bm{x}, t)}
		\end{equation}
	\end{block}
	
	\begin{columns}
		\begin{column}[b]{0.45\textwidth}
			\bi
			\item $h(\bm{x}, t)$ linear predictor which is the sum of partial effects $h_j(\bm{x}, t)$
			\item[]
			\ei
		\end{column}
		\begin{column}[b]{0.55\textwidth}
			\bi
			\item each $h_j(\bm{x}, t)$ is a real valued function over $\mathcal{T}$ and can depend on one or several covariates
			\item[]
			\ei
		\end{column}
	\end{columns}
	
%	\pause
%	\bigskip
%	$\ra$ Scalar response as degenerated case with $\mathcal{T}=[T_1,T_1]$
	
\end{frame}

% ------- loop ---------------------------------------

%%%%%%%%%%%%%%%%% generic model

\begin{frame}
\frametitle{\secname}
\framesubtitle{Generic model 'evolution'}
\begin{itemize}
	\item { 
		\only<1,2,3>{scalar response $Y\in\R$}
			\only<4->{functional response $Y(t)$, $t \in \mathcal{T} = [T_1, T_2]$}
		}
	\item {
		\only<1>{a single metric covariate $z\in\R$}}
			\only<2>{{\color{lmuGruen} vector of covariates $\bm{x}$} containing scalar covariates $z$\newline $\rightarrow$ metric or categorical}
				\only<3>{vector of covariates $\bm{x}$ containing scalar covariates $z$\newline $\rightarrow$ metric or categorical}
					\only<4-> {vector of covariates $\bm{x}$ containing {\color{lmuGruen} functional covariates $x(s)$} and scalar covariates $z$}
\end{itemize}

\begin{block}{
		\only<1>{Simple linear model}
			\only<2>{Linear model}
				\only<3>{Additive model}
					\only<4->{Functional additive model}}
	\begin{equation}
	\nonumber
	\only<1>{\mathbb{E}\left(Y\mid z \right) =  \beta_0 + \beta_1 z}
		\only<2>{\mathbb{E}\left(Y(t)\mid {\color{lmuGruen} \bm{x}}\right) =  h(\bm{x}) = {\color{lmuGruen} {\sum}_{j} \, \beta_j x_j}}
			\only<3>{\mathbb{E}\left(Y\mid \bm{x}\right) =  h(\bm{x}) = {\color{lmuGruen} {\sum}_{j} \, \only<3>{h_j(\bm{x})}}}
%				\only<4>{\mathbb{E}\left({Y(t)}\mid \bm{x}\right) =  h(\bm{x}, {t}) = {\sum}_{j} \, {h_j(\bm{x}, {t})}}
					\only<4->{\mathbb{E}\left({\color{lmuGruen}Y(t)}\mid \bm{x}\right) =  h(\bm{x}, {\color{lmuGruen}t}) = {\sum}_{j} \, {\color{lmuGruen}{h_j(\bm{x}, t})}}
	\end{equation}
\end{block}

\begin{columns}
	\begin{column}[b]{0.45\textwidth}
		\bi
		\item {
			\only<1>{with intercept $\beta_0\in\R$ and slope $\beta_1\in\R$}
				\only<2>{$h(\bm{x})$ linear predictor with coefficient vector $\bm{\beta}$}
					\only<3>{$h(\bm{x})$ linear predictor which is the sum of {\color{lmuGruen} partial effects $h_j(\bm{x})$}}
						\only<4->{$h(\bm{x}, t)$ linear predictor which is the sum of partial effects $h_j(\bm{x}, t)$}
		}
		\item[]
		\ei
	\end{column}
	\begin{column}[b]{0.55\textwidth}
		\bi
		\item<3-> {
			\only<3>{each $h_j(\bm{x})$ returns single value and can depend on one or several covariates}
				\only<4->{each $h_j(\bm{x}, t)$ is a real valued function over $\mathcal{T}$ and can depend on one or several covariates}
		}
		\item<3->[]
		\ei
	\end{column}
\end{columns}
%
%\bigskip
%\only<5> {$\ra$ Scalar response as degenerated case with $\mathcal{T}=[T_1,T_1]$}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% partial effects

\subsection{Covariate effects: scalar-on-function}

\frame{
	\frametitle{\subsecname}
	
	\bi
	\item<1-> functional linear effect $\int_{\mathcal{S}} x(s)\beta(s)\;ds$ \only<2> {\newline \hspace{2cm} $\rightarrow$ smooth functions like $\beta(s)$ modeled with basis functions}
	
	\item<3-> all usual scalar-on-scalar effects ...
	\item<3->[]
	\item<3->[]
	\item<3->[]
	\item<3->[]
	\ei
}


% ------------------------------------------

\subsection{Data of fossil fuel}


\addtocounter{framenumber}{-1}
\frame[plain]
{
	\begin{center}
		\textcolor{lmuGruen}{\LARGE {\bfseries Example!!!}}\\
		\textcolor{lmuGruen}{\large {\bfseries scalar-on-function}}
	\end{center}
}

% ------------------------------------------

\frame{
	\frametitle{\subsecname}
	Model equation:
	\begin{align*}
	y  &= \beta_0 + f(z_\ho) + \int_{\mathcal{S}_\NIR} x_\NIR (s_\NIR)  \beta_\NIR(s_\NIR) \,ds_\NIR + \\ 
	&\ \int_{\mathcal{S}_\UVVIS} x_\UVVIS (s_\UVVIS)  \beta_\UVVIS(s_\UVVIS) \,ds_\UVVIS + \varepsilon,
	\end{align*}
	\begin{itemize}
		\item heat value $y$
		\item non-linear effect of water content (H2O)
		\item linear functional effect of NIR and UV spectrum
	\end{itemize}
	
	\action<2> {
		\bigskip
		$\rightarrow$ For code for presented examples and more check out:\newline \hspace{1cm} {\color{lmuGruen} github.com/davidruegamer/StatisticalComputing2017}}
}

% -------------- ----------------- -----------------

\frame{
	\frametitle{Data of fossil fuel: results}
	
	Estimated effects with stopping iteration chosen by 10 fold bootstrap 
	
	\hspace*{-1.4cm}		\includegraphics[width = 1.25\textwidth]{figures/fuel_results-1.pdf} 
}

\frame{
	\frametitle{Data of fossil fuel: results}
	
	\begin{itemize}
		\item estimated effects on 100 bootstrap samples (gray lines) 
		\item point-wise median (black lines)
		\item point-wise 5 and 95\% quantiles (dashed red lines)
	\end{itemize}
	
	\hspace*{-1.4cm}	\includegraphics[width = 1.25\textwidth]{figures/sof_boot-1.pdf} 
}


% --------------- ------------------------ ----------------------- -----------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% partial effects

\subsection{Covariate effects: function-on-scalar}

\frame{
	\frametitle{\subsecname}
	\bi
	\item<1-> smooth intercept $\beta_0(t)$  
	\item<2-> group-specific smooth intercepts $\beta_{0a}(t)$  
	\only<3-> {\newline \hspace{1cm} $\rightarrow$ curve-specific smooth error $\xi(t)$ for each observation} 
	
	\bigskip
	\item<4-> smooth linear effect of scalar covariate $z\beta(t)$   
	\item<5-> smooth non-linear effect of scalar covariate $f(z,t)$     
	%\item<1-> smooth group-specific linear effect of a scalar covariate $z\beta_{1a}(t)$ 
	
	\bigskip
	\item<6-> interactions, e.g., $z_1 z_2 \beta(t)$ and $f(z_1, z_2, t)$ 
	\ei  
	
	\action<6->{
	\bigskip
	\begin{flushright}{\small \citep{scheipl2015,brockhaus2015}}\end{flushright}
	}
}

% -------------------------------------------


% ----------------- ----------------------- ------------------------ -----------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Covariate effects: function-on-function}

\frame{
	\frametitle{\subsecname}
	\vspace*{0.3cm}
	
	\bi
	\item<1-> concurrent effect \only<4>{\color{blue}}$x(t) \beta(t)$
	\vspace*{0.1cm}
	\item<2-> linear effect of functional covariate  \only<5>{\color{lmuGruen}} $\int_{\mathcal{S}} x(s)\beta(s,t)\;ds$
	\action<3->{
		\item constrained effect of functional covariate   \only<6>{\color{lmuGruen}} $\int_{l(t)}^{u(t)}\! x(s)\beta(s,t)\;ds$ \\
		%with integration limits $[l(t), u(t)]$
		%\textcolor{lmuGruen}{historical effect} 
		%$$\int_{T_1}^{t}\! x(s)\beta(s,t)\;ds$$
		%and concurrent effect $x(t)\beta(t)$
		\ei
	}
	\action<4->{
		\bi \item[]
		\renewcommand\arraystretch{1.4}
		\bigskip
		\begin{tabular}{l|rccc}
			%\hline
			effect & &  {\only<5>{\color{lmuGruen}} linear} &   {\only<6>{\color{lmuGruen}} historical} & 
			{\only<6>{\color{lmuGruen}} lag} 
			% & lead 
			% & concurrent 
			\\
			\hline
			$[l(t), u(t)]$ & & 
			{\only<5>{\color{lmuGruen}} $[T_1, T_2]$} & 
			{\only<6>{\color{lmuGruen}}  $[T_1, t]$} & %limits
			{\only<6>{\color{lmuGruen}} $[t - \delta, t]$} %& 
			% $[T_1, t - \delta]$ & 
			% $[t, t]$ 
			\\
			& t
			& \includegraphics[width=0.16\textwidth, page=1]{lim_signal.pdf}
			& \includegraphics[width=0.16\textwidth, page=1]{lim_hist.pdf}  
			& \includegraphics[width=0.16\textwidth, page=1]{lim_lag.pdf}
			% & \includegraphics[width=0.16\textwidth, page=1]{lim_lead.pdf}
			% & \includegraphics[width=0.16\textwidth, page=1]{lim_conc.pdf} 
			\\ 
			\multicolumn{1}{r}{}  & & & s & \\
			%\hline
		\end{tabular}
		\ei
		
		%\bigskip
		%\begin{flushright}{\small (see Scheipl et al., 2015; Brockhaus et al., 2015/2016a)}\end{flushright}
		
		\begin{flushright}{\small \citep{scheipl2015,brockhaus2015hist,brockhaus2016}}\end{flushright}
	}
}

% ----------------------- --------------------- ------------------------ -----------------------


\frame{
	\frametitle{\subsecname}
	\framesubtitle{Interactions} 
	
	%\bigskip
	%Interactions between scalar and functional covariates
	\action<+->{
		\bi 
		\item linear interaction of scalar and functional covariate $$z \int_{l(t)}^{u(t)} x(s) \beta(s,t) ds$$
		\item group-specific functional effects $$I(z=a) \cdot \int_{l(t)}^{u(t)} x(s) \beta_a(s,t) ds $$\\ with indicator function $I(\cdot)$
		\ei 
	}
	
%	\bigskip
%	\bigskip
%	\bi
%	\item[$\rightarrow$]<+-> For all the listed effects ensure identifiability by suitable constraints
%	\ei
}

% --------------- ------------------------- ------------------- -------------------


% ------------------------------------------

\addtocounter{framenumber}{-1}
\frame[plain]
{
	\begin{center}
		\textcolor{lmuGruen}{\LARGE {\bfseries Example!!!}}\\
		\textcolor{lmuGruen}{\large {\bfseries Functional-on-function}}
	\end{center}
}

% -------------------------------------------


\subsection{Emotion components data}

\begin{frame}
\frametitle{\subsecname}

Data set from \citet{Gentsch.2014}, also used in \citet{rugamer2016}
\vspace*{0.2cm}
\begin{itemize}
	\item Main goal: Understand how emotions evolve
	\item Participants played a gambling game with real money outcome
	\item Emotions ``measured'' via EMG (muscle activity in the face)
	\item Influencing factor \emph{appraisals} measured via EEG (brain activity)
	\item Different game situation, a lot of trials
\end{itemize}
\end{frame}

% ------------ ----------------- -------------------- ----------------------- -------------------

\frame{
	\frametitle{\subsecname}
	\begin{figure}
		\includegraphics[width=\columnwidth]{figures/explData-1}
	\end{figure}
	\vspace*{-0.3cm}
	\hfill  \citep{brockhaus2017}
	
	Goal:  Try to explain 
	\begin{itemize}
		\item facial expressions (measured with EMG) 
		\item by brain activity (measured with EEG) 
	\end{itemize}
	$\rightarrow$ Function-on-function-regression 
}

% -----------------------------------------

\begin{frame}
\frametitle{\subsecname}
Model equation:
\begin{overlayarea}{\textwidth}{1.5cm}
	\only<1>{$$y_\EMG(t) = \beta_0(t) + x_\EEG(t) \beta_1(t) + \eps(t)$$}
	%\only<2-4>{$$y_\EMG(t) = \beta_0(t) + \textcolor{lmuGruen}{\int}_{\,}^{\,} x_\EEG(\textcolor{lmuGruen}{s}) \beta_1(\textcolor{lmuGruen}{s}) \textcolor{lmuGruen}{\drm s} + \eps(t)$$}
	\only<2>{$$y_\EMG(t) = \beta_0(t) +  \textcolor{red}{\int}_{\,}^{\,} x_\EEG(s) \beta_1(s, t) \drm s + \eps(t)$$}
	\only<3>{$$y_\EMG(t) = \beta_0(t) + \int_{\textcolor{blue}{0}}^{\textcolor{blue}{t-\delta}} x_\EEG(s) \beta_1(s,t) \drm s + \eps(t)$$}
\end{overlayarea}
\begin{itemize}
	\item<+-> One-to-one relation between EEG and EMG $\rightarrow$ Concurrent effect
	%\item<+-> Incorporate all time points $\rightarrow$ \textcolor{lmuGruen}{Constant functional effect}
	%\begin{itemize}
	%\item<+-> The effect of EEG does not change over time $t$
	%\item<+-> Every time point of EEG can have an influence on EMG
	%\end{itemize}
	\item<+-> Cumulated effect of functional covariates $\rightarrow$ \textcolor{red}{Linear functional effect}
	\item<+-> EMG can only be influenced by EEG activities in the past\\ $\rightarrow$ \textcolor{blue}{Historical effect}
\end{itemize}
\end{frame}

\begin{frame}{Results for more complex model}
\includegraphics[width=0.85\textwidth]{figures/resultPlots1a-1}
\end{frame}

% ---------------------------------------------

\section{Estimation approaches}
\subsection{Option 1: penalized likelihood}

\begin{frame}{Estimation \subsecname}

\vspace*{0.2cm}



\vspace{.5cm}

\begin{block}{Alternative model representation}
	\begin{equation}
	\nonumber
	Y(t) =  h(\bm{x}, t) + {\color{lmuGruen} \xi(t)} + \varepsilon(t)
	\end{equation}
\end{block}

\begin{itemize}
	\item curve specific smooth random error {\color{lmuGruen}$\xi(t)$} \newline
			\hspace{2cm} $\rightarrow$ 'functional random intercept'
	\item random white-noise measurement error $\varepsilon(t)$
\end{itemize}

\begin{itemize}
	\item<2->[$\Rightarrow$] also $\xi(t)$ modeled via basis representation
	\item<3>[$\Rightarrow$] trade-off between curve-specific error and global covariate effects via {\color{lmuGruen} mixed model type fitting strategies}
\end{itemize}

\end{frame}

% ---- ----- -----

\begin{frame}{Estimation \subsecname}

\begin{itemize}
	\item implementation in the R package {\color{lmuGruen}\texttt{refund}} (Goldsmith et. al. 2018) based on \texttt{mgcv} (Wood, 2017)
		\begin{itemize}
			\item function \texttt{pfr} for scalar-on-function
			\item function \texttt{pffr} for function-on-function
		\end{itemize}
	\item<2-> ready for
		\begin{itemize}
			\item<3-> irregular / sparse functional data
			\item<4-> functional responses with measurement errors \newline
					\hspace{1cm} $\rightarrow$ no basis representation of functional observations but only of effect functions
			\item<5-> multiple levels of dependence in the data\newline
					\hspace{1cm} $\rightarrow$ functional mixed models for crossed designs (R package \texttt{sparseFLMM}, Cederbaum 2017)
		\end{itemize}
\end{itemize}

\end{frame}

% ---------------------------------------------


\subsection{Option 2: gradient boosting}

\begin{frame}{Estimation \subsecname}


\vspace*{0.1cm}

Idea:

\bi
\item iteratively \textcolor{lmuGruen}{boost} the model performance\\
($\hat{=}$ reduce expected quadratic-loss) %of base-learners (simple models) %by iteratively shifting the focus towards difficult observations.  
\item<+-> by fitting and evaluating the partial effects component-wise 
\item<+-> using one partial effect at a time to update the model
\ei 
\bigskip

\action<+->{$\ra$ Results in component-wise gradient descent steps}

\action<3>{
	\vspace*{-0.45cm}
	\begin{figure}
		\includegraphics[width=\textwidth]{figures/graddes.png}
	\end{figure}
	\vspace*{-2.3cm}
	\begin{tiny}
		\hfill Source: \url{https://github.com/joshdk/pygradesc}
	\end{tiny}
}

\only<4>{
	\vspace{-3cm}
	To account for in-curve dependency:
	\begin{itemize}
		\item curve specific smooth error functions
		\item curve-wise cross-validation
	\end{itemize}
}


\end{frame}

% ---- ----- -----

\begin{frame}{Estimation \subsecname}
Implemented in 
\bi 
\item \textsf{R} package \textcolor{lmuGruen}{\texttt{FDboost}} \citep{FDboost_gen}
\item based on \textsf{R} package \texttt{mboost} \citep{mboost_gen}
\item Extension to GAMLSS based on \texttt{gamboostLSS} \citep{gamboostLSS2015}
% \item efficient implementation based on generalized linear array models (Currie et al., 2006)
\ei

\vspace{1cm}

\action<2-> {Major differences to Option 1 penalized likelihood estimation:}
\bi 
\item<2->[$\boldsymbol{-}$] no asymptotic inference (confidence intervals, ...), or at least not that easy 
\item<3->[$\boldsymbol{+}$] very flexible in terms of fitting criteria
\item<4->[$\boldsymbol{+}$] much faster for many complex covariate effects
\item<5->[$\boldsymbol{+}$] inherent variable selection
\ei

\end{frame}



% +++++++++++++++++ +++++++++++++++++++ +++++++++++++++++ +++++++++++++++++++++++


\section{More generalizations...}
\subsection{Generalized Functional Additive Models}

\frame{
	\frametitle{More generalizations...}
	\begin{itemize}
		\item functional response $Y(t)$, $t \in \mathcal{T} \subset \mathbb{R}$
		\item vector of covariates $\bm{x}$ containing functional covariates $x(s)$ and scalar covariates $z$
	\end{itemize}
	
	\begin{block}{\textbf{Generalized} functional additive models}
		\begin{equation}
		\nonumber
		\mathbb{E} \left(Y(t)\mid \bm{x}\right) = {\color{lmuGruen} g\big(} h(\bm{x},t) {\color{lmuGruen} \big)} = {\color{lmuGruen} g\big(}{\sum}_{j} \, h_j(\bm{x},t) {\color{lmuGruen}\big)}
		\end{equation}
	\end{block}
	
	\begin{columns}
		\begin{column}[b]{0.45\textwidth}
			\bi
			\item $h(\bm{x},t)$ linear predictor which is the sum of partial effects $h_j(\bm{x},t)$
			\item[]
			\ei
		\end{column}
		\begin{column}[b]{0.55\textwidth}
			\bi
			\item each $h_j(\bm{x},t)$ is a real valued function over $\mathcal{T}$ and can depend on or several covariates
			\item[]
			\ei
		\end{column}
	\end{columns}
	
	$\Rightarrow$ Different response distributions for e.g. {\color{lmuGruen} positive- or integer-valued response functions} with link function \color{lmuGruen} $g$	
}

% ----------------------------------------------

\subsection{Generalizations beyond mean regression}

\frame{
	\frametitle{More generalizations...}
	
	With \texttt{FDboost} also 
	\vspace{.3cm}
	\begin{itemize}
		\item median regression
		\item quantile regression
		\item functional GAMLSS \citep{rigby2005}, i.e. simultaneous regression for multiple parameter functions
	\end{itemize}
	
}	
	
% +++++++++++++++ +++++++++++++++ +++++++++++++++++++++++ ++++++++++++++++++++++

\section{Summary}

\frame{
	\frametitle{Summary}
	
	\begin{itemize}
		\item Three types of functional regression: scalar-on-function, function-on-scalar, and function-on-function\newline 
		\hspace{2cm} $\rightarrow$ focus on functional observations as a whole
		\item different types of regression models existing\newline 
		\hspace{2cm} $\rightarrow$ here, focus on a framework generalizing the linear model
		\item suitable for various data and model scenarios
		\item two estimation procedures available
		\newline 
		\hspace{2cm} $\rightarrow$ \texttt{refund}: offers asymptotic inference
		\newline 
		\hspace{2cm} $\rightarrow$ \texttt{FDboost}: extremely flexible + inherent variable selection
	\end{itemize}
	
}	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\appendix
\backupbegin

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{Bibliography}

\begin{frame}[allowframebreaks]
\frametitle{References}
{
	\footnotesize
	\bibliographystyle{plainnat}
	\bibliography{literatur_entries}
	
}

\end{frame}


% ------------------------------------------------------


\begin{frame}{Implementation in \texttt{FDboost}}

\bi
\item Main fitting function: \\
\vspace*{0.1cm}
\quad \texttt{FDboost(formula, timeformula, data, ...)}
\vspace*{0.2cm}
%\begin{Schunk}
%\begin{Sinput}
%\begin{verbatim}
%FDboost(formula, timeformula, id = NULL, 
%                  numInt = 'equal', data, offset = NULL, ...)
%\end{verbatim}
%\end{Sinput}
%\end{Schunk}

\item\texttt{timeformula}
\bi
\item[]\texttt{= NULL} for scalar-on-function regression,  
\item[]\alert<.(2)>{\texttt{= $\sim$ bbs(t)}} for function-on-function regression
\ei
\vspace*{0.2cm}
\item Some of the base-learners for functional data:\\
\vspace*{0.2cm}
\renewcommand\arraystretch{1.6}
\begin{small}
	\begin{tabular}{ll}
		$z \beta(t)$ & \,\,\texttt{bolsc(z)} \only<2>{\textcolor{lmuGruen}{\texttt{\%O\% bbs(t)}}} \\
		$f(z,t)$ &  \,\,\texttt{bbsc(z)} \only<2>{\textcolor{lmuGruen}{\texttt{\%O\% bbs(t)}}}\\
		$z_1 z_2 \beta(t)$ & \,\,\texttt{bols(z1) \%Xc\% bols(z2)} \only<2>{\textcolor{lmuGruen}{\texttt{\%O\% bbs(t)}}}\\
		$\int_\mathcal{S} x(s) \beta(s,t) ds$ & \,\,\texttt{bsignal(x, s = s)} \only<2>{\textcolor{lmuGruen}{\texttt{\%O\% bbs(t)}}} \\
		$x(t) \beta(t)$ & \,\,\texttt{bconcurrent(x, s = s, time = t)}\\
		$\int_{l(t)}^{u(t)} x(s) \beta(s,t)ds$ & \,\,\texttt{bhist(x, s = s, time = t, limits = ...)}\\
		%$z \int_{T_1}^{t} x(s) \beta(s,t)ds$ & \,\,\texttt{bolsc(z) \% X \% bhistx(x, s = s, time = t, limits = , ...)}
	\end{tabular}
\end{small}

\ei

\end{frame}

% ------------ ------------------- ----------------- --------------------

\begin{frame}[fragile]{Example: \texttt{FDboost} call}

\begin{verbatim}
FDboost(EMG ~ 1 + 
brandomc(id, df = 5) +
bhist(EEG, df = 20),
timeformula = ~ bbs(t, df = 4), 
control = boost_control(mstop = 5000, 
trace = TRUE), 
data = data)
\end{verbatim}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\frame{
	\frametitle{Specification of partial effects}
	{\small The generic model: 
		$\mathbb{E}(Y(t)| \bm{x}) =  h(\bm{x},t) = {\sum}_{j} \textcolor{lmuGruen}{h_j(\bm{x},t)}$
	}
	
	\bigskip
	\begin{block}{Row tensor product basis}
		\begin{equation}
		\nonumber
		h_j(\bm{x},t) = \left\{ \mb_{j}(\bm{x},t)^\top \odot \mb_Y(t)^\top \right\} \mtheta_j
		\end{equation}
	\end{block}
	%
	\begin{itemize} 
		\item $\mb_{j}$ / $\mb_Y$ vector of $\kappa_j$/$\kappa_Y$ basis functions in covariates / over $\mathcal{T}$ 
		\item $\odot$ row-wise tensor product ('Kronecker product on rows')
		%\item $\otimes$ Kronecker product 
		\item $\mtheta_j$ coefficient vector 
	\end{itemize}
	%\pause
	\begin{itemize}
		\item Ridge-type penalty 
		with penalty term $\mtheta_j^\top \mathbf{P}_{j Y} \mtheta_j$ 
		for regularization in both directions 
		%\\ 
		%penalty e.g.~$\mathbf{P}_{j Y} =  \lambda_{j} (\mathbf{P}_{j} \otimes \mathbf{I}_{K_Y}) +  \lambda_Y ( \mathbf{I}_{K_{j}} \otimes \mathbf{P}_{Y})$
		%\begin{itemize}
		%\item with $\mathbf{P}_{j}$, $\mathbf{P}_Y$ penalty matrices for $\mb_{j}$/$\mb_Y$
		%\item $\lambda_{j}$, $\lambda_Y \geq 0$ smoothing parameters 
		%\end{itemize}
		
		\pause 
		\bigskip
		\item if possible, representation as \textcolor{lmuGruen}{generalized linear array model} \\ \citep{currie2006}. 
	\end{itemize}
	
	%%%%% evtl eine neue Folie machen fuer array Modelle
	%\bi 
	%\item if possible use array framework of Currie et al., (2006), so-called functional linear array model (Brockhaus et al., 2015) $\ra$ more efficient computation  
	%\ei
} 
\note{
	now let's go into detail for the additive predictor
	\bi
	\item each effect is represented as tensor product basis of \textbf{two marginal bases}
	\item one for the covariates, and another over time, coefficient vector
	%\item possible to represent as \textbf{Kronecker} product only if the response is observed over a common grid and the effect can be split into two marginal bases where the basis in the covariates does not depend on $t$!
	\item for \textbf{regularization}: Ridge-type penalty and suitable penalty matrix that penalizes an effect in the direction of the covariates and of time; \\
	the penalty induces: similar effects for similar values of the corresponding covariates and similar effects for close time-points of the response; smoothness assumptions 
	\item if possible we represent the model as generalized linear array model, which saves time and memory when computing the model 
	\ei 
}

% ++++++++++++++++++ +++++++++++++++++++++++ ++++++++++++++++++++++ ++++++++++++++++++++++

\backupend

\end{document}

